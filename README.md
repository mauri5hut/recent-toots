## Recent Toots Plugin:

Minimum Bludit Version: v2.3.x

Display recent toots on the sidebar.

### Instructions:
* Copy the plugin to `bl-plugins`.
* Activate it.
* Configure URL, Refresh rate and number of Toots in Plugin-Settings

### Additional Notes:

* Plugin only tested in Theme "andy".