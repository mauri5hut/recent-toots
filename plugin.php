<?php

class RecentToots extends Plugin
{

    public function init()
    {
        $this->dbFields = array(
            'feedUrl'=>'',
            'cacheMaxAge'=>0,
            'nrOfToots'=>0,
        );
    }

    public function form()
    {
        global $L;

        $html = '<div>';
        $html .= '<label>'.$L->get('Feed URL').'</label>';
        $html .= '<input id="feedUrl" name="feedUrl" type="text" value="'.$this->getValue('feedUrl').'">';
        $html .= '<span class="tip">'.$L->get('https://mastodon-instance-xy/users/yourusername.rss').'</span>';
        $html .= '<br>';

        $html .= '<label>'.$L->get('Refresh rate').'</label>';
        $html .= '<input id="cacheMaxAge" name="cacheMaxAge" type="text" value="'.$this->getValue('cacheMaxAge').'">';
        $html .= '<span class="tip">'.$L->get('Refresh rate of toots in seconds').'</span>';
        $html .= '<br>';

        $html .= '<label>'.$L->get('Show Toots').'</label>';
        $html .= '<input id="nrOfToots" name="nrOfToots" type="text" value="'.$this->getValue('nrOfToots').'">';
        $html .= '<span class="tip">'.$L->get('Number of Toots shown').'</span>';
        $html .= '</div>';

        return $html;
    }

    public function siteSidebar()
    {
        // RSS-Feed-URL
        $feedUrl = $this->getValue('feedUrl');

        // Cache file for RSS content
        $cacheFile = __DIR__.'/feed_cache.html';

        $feedLogo = 'bl-plugins/recent-toots//mastodon.png';

        // Cache max age in seconds
        $cacheMaxAge = $this->getValue('cacheMaxAge');

        // Number of entries from RSS content
        $nrOfToots = $this->getValue('nrOfToots');

        if(!file_exists($cacheFile) or filemtime($cacheFile) < (time() - $cacheMaxAge)) {

            $xml = simplexml_load_string(file_get_contents($feedUrl));
            $entries = $xml->channel->item;
            $counter = 0;

            $html  = '<div class="plugin plugin-recent-toots">';
            $html .= '<div class="plugin-content">';
            $html .= '<p"><img src="'.htmlspecialchars($feedLogo).'" border="0" align="left" width="50" height="50" loading="lazy" style="margin-right: 10px">Neuste Toots</p>';

            foreach($entries as $root) {

                $counter++;

                if($counter > $nrOfToots) {
                    break;
                }

                $html .= '<hr style="height:1px;border-width:0;color:rgb(160, 160, 160);background-color:rgb(160, 160, 160)">';

                $date = date('d.m.Y', strtotime($root->pubDate));
                $html .= '<p style="font-size: small">'.htmlspecialchars($date).'</p>';

                $description = strip_tags($root->description);
                $html .= '<a class="mastodon-timeline" href="'.htmlspecialchars($root->link).'" style="font-size: small">'.htmlspecialchars($description).'</a>';
            }

            $html .= '</div>';
            $html .= '</div>';

            file_put_contents($cacheFile, $html);

            return $html;
        }
        else {
            echo file_get_contents($cacheFile);
        }
    }
}